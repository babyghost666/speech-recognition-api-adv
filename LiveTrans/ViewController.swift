//
//  ViewController.swift
//  LiveTrans
//
//  Created by Peter Leung on 11/9/2016.
//  Copyright © 2016 winandmac Media. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class ViewController: UIViewController {

    @IBOutlet weak var baseLabel: UILabel!
    @IBOutlet weak var baseButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func RequestRecordPermission(){
        AVAudioSession.sharedInstance().requestRecordPermission() {
            [unowned self] allowed
            in
            DispatchQueue.main.async {
                if allowed {
                    self.RequestTransPermission()
                } else {
                    self.ErrorText()
                }
            }
        }
    }
    
    func RequestTransPermission(){
        SFSpeechRecognizer.requestAuthorization { [unowned self] authStatus in
            DispatchQueue.main.async {
                if authStatus == .authorized {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.ErrorText()
                }
            }
        }
    }

    
    func ErrorText(){
        baseLabel.text = "Permission Disabled😨"
        
        self.baseButton.isEnabled = false
        UIView.animate(withDuration: 2.0) { 
            self.baseButton.alpha = 0.3
        }
        
        
    }
    
    @IBAction func GrantBtnClick(_ sender: AnyObject) {
       RequestRecordPermission()
        
    }

}

