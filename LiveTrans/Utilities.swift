//
//  Utilities.swift
//  LiveTrans
//
//  Created by Peter Leung on 12/9/2016.
//  Copyright © 2016 winandmac Media. All rights reserved.
//

import Foundation

class Utilities {
    static func getDocsDirectory() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDir = paths[0]
        return docsDir
    }
    
    static func getAudioFile() -> URL? {
        do {
            let audioURL = try getDocsDirectory().appendingPathComponent(getDateAndTime() + ".m4a")
            return audioURL
        } catch _ {
            return nil
        }
    }
    
    static func getDateAndTime() -> String{
       
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-mm-dd-HH-mm-ss"
        let date = Date()
        let timeString = formatter.string(from: date)
        return timeString
    }
}

    
