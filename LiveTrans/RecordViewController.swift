//
//  RecordViewController.swift
//  LiveTrans
//
//  Created by Peter Leung on 12/9/2016.
//  Copyright © 2016 winandmac Media. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class RecordViewController: UIViewController, AVAudioRecorderDelegate {
    
    var audioRecorder: AVAudioRecorder?
    var recFileLink: URL!
    
    var audioPlayer: AVAudioPlayer?

    @IBOutlet weak var activityIndicate: UIActivityIndicatorView!
    @IBOutlet weak var TranscribedText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        recFileLink = Utilities.getAudioFile()
        print("File Will Be Recorded at \(recFileLink!.absoluteString)")
        recordAudio()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func recordAudio(){
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
            try session.setActive(true)
            
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100,
                AVNumberOfChannelsKey: 2,
                AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
            
            audioRecorder = try AVAudioRecorder(url: recFileLink, settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.record()
            activityIndicate.startAnimating()
        } catch let error {
            print("Error Recording \(error)")
            recordEnded(success: false)
        }
    }
    //Audio Delegate
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag{
            recordEnded(success: true)
        } else {
            recordEnded(success: false)
        }
    }
    
    func recordEnded(success: Bool){
        audioRecorder?.stop()
        if success {
            do {
                //Play audio after recording
                audioPlayer?.stop()
                audioPlayer = try AVAudioPlayer(contentsOf: recFileLink)
                audioPlayer?.play()
                transcribeAudio()
            } catch let error {
                print("Error Recording at recordEnded func \(error)")
            }
        }
    }

    @IBAction func stopRecordButton(_ sender: AnyObject) {
        audioRecorder?.stop()
        sender.titleLabel??.text = "DONE"
        activityIndicate.stopAnimating()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //in case the audio keep on playing when the home button is pressed
        audioPlayer?.stop()
    }
    
    //Transcribed Functions
    func transcribeAudio(){
        let recogniser = SFSpeechRecognizer()
        let request = SFSpeechURLRecognitionRequest(url: recFileLink)
        recogniser?.recognitionTask(with: request, resultHandler: { (result, error) in
            guard let result = result else {
                print("Error")
                return
            }
            
            if result.isFinal{
                let text = result.bestTranscription.formattedString
                self.TranscribedText.text = text
                
            }
        })
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
